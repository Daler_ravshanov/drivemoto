import { createRouter, createWebHistory } from 'vue-router'
import HomePage from '../pages/HomePage.vue'
import ProductsPage from '../pages/ProductsPage.vue'
import ProductPagesTwo from '../pages/ProductPagesTwo.vue'

const routes = [
  {path: '/', component: HomePage},
  {path: '/products', component: ProductsPage},
  {path: '/productss', component: ProductPagesTwo}
]

const router = createRouter({
  history: createWebHistory(),
  routes
})
export default router